Source: zegrapher
Section: science
Priority: extra
Maintainer: Georges Khaznadar <georgesk@debian.org>
Build-Depends: debhelper (>= 9.0.0),
 qt5-qmake, qtchooser, qtbase5-dev, libqt5webkit5-dev, qtbase5-dev-tools,
 libboost-dev
Standards-Version: 4.1.1
Homepage: http://zegrapher.com/
Vcs-Browser: https://salsa.debian.org/georgesk/zegrapher
Vcs-Git: https://salsa.debian.org/georgesk/zegrapher.git

Package: zegrapher
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: plotting mathematical functions and sequences on the plane
 ZeGrapher is developed with the idea that it must be as easy to use as
 possible, suitable for high school students.
 Here are features offered by Zegrapher:
 .
 * Visualize up to six functions simultaneously. All the usual functions
    can be called, they're all written on the virtual keyboard of the
    program. The name of these must be followed by an open parenthesis.
    Ex : cos(3x), sqrt(x) and not cos 3x or sqrt x. On a given function,
    others can be called, and so it's possible to associate or compose
    functions.
 * Numerical sequence plotting, they can be defined by recurrent relation,
   or explicitly. On the recurrent definition, you can put as much first
   values as you need to.
 * Parametric equation plotting, with the possibility to use a second
   parameter, which is "k". The curves can be animated, with adjustable
   smoothness (frame rate) and speed (ms per step).
 * Parametric function and sequence plotting. The parameter to use is "k".
   Once you put it on an function's expression, new widgets will appear
   on which you'll enter the range and the step of "k". You can also
   choose two colors and each curve would take a color between these two.
 * Draw tangents, they can be moved and resized simply with the mouse.
 * Draw derivatives and antiderivatives of functions.
 * Print the graph, with these options :
  °  Choose the basis scale in centimeters, so you can measure the graph
     on the sheet with a ruler.
  °  Choose the size and the position of the graph on the sheet.
  °  Print in color or gray-scale.
  °  Export on a PDF file.
 * Exporting, it's possible to use all the known formats (png, jpeg,
   tiff, gif, ...).
 * Display a table of values, you can do it with three different options:
  °  From current graph view, the program will display the different
     functions ,sequences and parametric equation values from the graph's
     scale. And if you move the graph, the table would update itself
     automatically.
  °  Manual, you choose a number of empty boxes, then you'll enter by 
     yourself the values that you want to know.
  °  Semi-automatic, with a given start value, step and number of values
     to display.
 * Navigate on the graph :
  °  Select a curve to display the coordinates of its points.
  °  Zoom/unzoom on each axis separately or together, with the mouse
     on a point, or on a rectangular region.
  °  Move the graph.
 * Customize the presentation :
  °  Change all the colors: axes, background, functions...
  °  Adjust curves displaying precision, it'll also affect rendering speed.
  °  Show/hide the grid.
  °  Activate/deactivate curves' smoothing.
  °  Display the graph on an orthonormal basis.
