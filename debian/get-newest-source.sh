#!/bin/sh

package=zegrapher

version=$2

orig_tgz=${package}_${version}.orig.tar.gz
orig_txz=${package}_${version}.orig.tar.xz
untarDir=$(tar tzf ../${orig_tgz}| head -n 1)

(cd ..; tar xzf ${orig_tgz})
rm ../${orig_tgz}
(cd ..; mv ${untarDir} ${package}-${version}; tar cJf ${orig_txz} ${package}-${version})

echo "Created ../${orig_txz} and ../${package}-${version}"



